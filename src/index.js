import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();


/**
 * GENERAR CORREO TEMPORAL: https://dropmail.me/es/
 * CORREO TEMPORAL: lujsgjbo@supere.ml
 * PASS OF SURGE: 123
 * SURGE: https://surge.sh/
 * LINKS OF PAGES: http://sweet-shake.surge.sh/
 *                 http://unbecoming-apparel.surge.sh/
 * 
 */